# Personal Blog

[![Netlify Status](https://api.netlify.com/api/v1/badges/fb969c51-9555-4d4a-bae8-4e8878601741/deploy-status)](https://app.netlify.com/sites/hardcore-torvalds-ca206b/deploys)


## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].
