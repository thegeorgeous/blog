+++
title = "Projects"
slug = "projects"
+++

## Open Source

   - [graphd](https://github.com/thegeorgeous/graphd) - A Ruby client for [DGraph](https://github.com/dgraph-io/dgraph) that uses [gRPC](https://grpc.io/)
   - [writeas](https://github.com/thegeorgeous/writeas) - A Ruby client for the [Write.as](https://write.as) API
   - [Flask-CQLAlchemy](https://github.com/thegeorgeous/flask-cqlalchemy) - A SQLAlchemy like Cassandra adapter for Flask
   - [railscasts-reloaded](https://github.com/thegeorgeous/railscasts-reloaded-theme) - An Emacs port of the Ryan Bates's famous Railscasts theme

## Contributions

   - [Gitlab](https://gitlab.com/gitlab-org/gitlab/-/commits/master?author=George%20Thomas)
   - [pgcli](https://github.com/dbcli/pgcli/commits?author=thegeorgeous)
