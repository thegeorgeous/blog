---
date: "2015-05-21T00:00:00Z"
title: Using SSH the right way
---
I hate PUTTY. There I said it. It is difficult to use. It look like something a
Windows user would use to connect to remote systems because the default terminal
sucks. As a proud Linux user(though Ubuntu; pardon me, Linux puritans),
I have always loved the terminal. Typing commands, and the reams of text output
that comes gives me a satisfying feeling. If I can connect to from the terminal
without much hassle, why would I use a stylised application that doesn't have a
normal copy and paste option?

So how do you setup the whole apparatus that will let you ssh into a remote
system? Here goes:
(I'm using an Amazon EC2 instance as an example)

* First of all find the `.ssh` folder in your home folder. If you don't have a
  config file in it, create one
* In the config file, add the following lines:
  ```
  Host *host-nickname*
        HostName *your-remote-server-ip*
        User *your-username*
        Identityfile *your-pem-file*
```
That's it, done! Now you can simply type `ssh host-nickname` to access the
remote shell. For other remote hosts, the Identityfile may be replaced with a
password.

This is a much better way than running `ssh -i your-pem-file your-username@your-remote-server-ip`
because of the following reasons:

* No need to type long commands. You could of course use aliases, but this is
  much more cleaner. Mainly because, the aliases don't need to be loaded when
  the shell loads.
* `scp` is much more easier. Simpy run `scp host-nickname:/your/remote/file/location /your/local/file/location`
  to download files.
* Editing remote file using emacs becomes possible.
