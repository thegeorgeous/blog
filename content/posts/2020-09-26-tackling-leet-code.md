---
date: "2020-09-26T00:00:00Z"
title: Tackling leetcode
draft: true
---

- Leetcode can be a confidence killer
- The marked difficulty of the problem can be deceptive
- The difficulty level assumes you are familiar with that area/algorithm/DS

