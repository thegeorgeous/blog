---
date: "2015-01-12T00:00:00Z"
title: Setting up a Jekyll blog on Github
---
According to Github setting up a blog on github pages is very easy, which I was
prepared to believe. Uploading an HTML file to the repo
[thegeorgeous.github.io](http://thegeorgeous.github.io) sets up a site on its
own. So a blog using Jekyll not only seemed to be natural step up but also as
easy as adding a markdown file in a newly created *_posts* folder. The problem
was when I wanted to see what it would look like once I uploaded it.

Github's help file recommended installing Jekyll on my laptop for this. It also
gave what seemed very detailed instructions on how to set it up. But therein lay
the problem. Thanks to the infernal way the whole Ruby development community has
been structured, it seemed near impossible to find the proper way to getup a
static html page generator up and running.

I work on a Ubuntu 14.10 machine *and* I am a stickler for updates. If it is not
the latest library or language version, I feel I am missing out the features
that the developers intended others to have. Not to mention my paranoia about
bugs and security flaws in legacy versions.

The problem? There are gems that work for some specific versions of ruby and
some that do not work for the same versions. A developer is given the ardous
tasks of identifying a ruby version and corresponding gems that can mutually
tolerate each other. Any mistake in this configuration and you are done for.

The best way to handle this environment of gems is no doubt *bundler* which
however is again available as, wait for it, *a gem*. (For the newbies, gems are
just ruby packages similar to python packages available on PyPI.)

So now you think all you need is to specify the gems you want in a Gemfile
required by bundler and you are done.

**No**

Your OS may not have the ruby required by the specified gems. So you have to
install the Ruby Version Manager
My first problem started with the sheer number of Ruby version out in the wild.
This is what `rvm list known` gave me:

```shell
# MRI Rubies
[ruby-]1.8.6[-p420]
[ruby-]1.8.7[-head] # security released on head
[ruby-]1.9.1[-p431]
[ruby-]1.9.2[-p330]
[ruby-]1.9.3[-p551]
[ruby-]2.0.0[-p598]
[ruby-]2.1.4
[ruby-]2.1[.5]
[ruby-]2.2.0
[ruby-]2.2-head
ruby-head

# for forks use: rvm install ruby-head-<name> --url https://github.com/github/ruby.git --branch 2.1

# JRuby
jruby-1.6.8
jruby[-1.7.18]
jruby-head

# Rubinius
rbx-1.3.3
rbx-2.2.10
rbx[-2.4.1]
rbx-head

# Opal
opal

# Minimalistic ruby implementation - ISO 30170:2012
mruby[-head]

# Ruby Enterprise Edition
ree-1.8.6
ree[-1.8.7][-2012.02]

# GoRuby
goruby

# Topaz
topaz

# MagLev
maglev[-head]
maglev-1.0.0

# Mac OS X Snow Leopard Or Newer
macruby-0.10
macruby-0.11
macruby[-0.12]
macruby-nightly
macruby-head

# IronRuby
ironruby[-1.1.3]
ironruby-head
```
For github pages, thankfully they specified that I had to install v2.0.0. Ah,
ever so helpful. Oh, wait, you are using not using a login shell. Sorry RVM
cannot work without a login shell. So you end up modifying your **.bashrc** file
to add the following line:

```shell
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
```

And then finally `bundle exec install` started installing the required gems and
despite being an atheist, I found myself thanking God.
