---
date: "2016-06-07T00:00:00Z"
title: Getting a cleaner Emacs configuration
---
[use-package](https://github.com/jwiegley/use-package) is a configuration
management package for Emacs. Given that emacs users tend to install a lot of
packages as well as add customisations for it, an average `init.el` can look like
a mess of macros and variable assignements. `use-package` provides a macro that
can be used to classify and arrange configurations for a package under one macro
This results in a much cleaner configuration.

Usage
-----
A simple declaration looks like this

```elisp
(use-package helm)
```

It is possible to add configuration to this like this:

```elisp
(use-package helm
	:config
	(helm-mode-1))
```

Need to add keybindings? `use-package` has you covered:

```elisp
(use-package helm
	:bind (("M-x" . helm-M-x)))
```

Want to hide minor modes that you know are always enabled? No problemo

```elisp
(use-package helm
	:diminish helm-mode)
```

And this is my favourite one. Need to ensure a package is installed before you
start using it?

```elisp
(use-package helm
	:ensure t)
```

This is my favourite because this makes your Emacs configuration ready to use
on a new machine. Just clone your config to the new machine and startup Emacs.
That's it, you're done. `use-package` will make sure the packages are installed.

Of course that also means `use-package` needs to be installed beforehand. That
is pretty easy. All you need to do is add these lines:

```elisp
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
```

Using `use-package` you can create a sane version of your Emacs configuration
as well as make it shareable
