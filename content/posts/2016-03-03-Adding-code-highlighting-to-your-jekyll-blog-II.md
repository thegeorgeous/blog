---
date: "2016-03-03T00:00:00Z"
title: Adding code highlighting to your jekyll blog - II
---

Last year I wrote on [Adding code highlighting to a jekyll blog](http://thegeorgeous.com/2015/02/03/Adding-code-highlighting-to-your-jekyll-blog.html)
using `highlight.js`. One of the main reasons I chose it was the number of
themes it provided, and being a fan of monokai, I was happy to see it in the
list of themes provided. That was until I languages became an issue for me.

Recently I started hacking my emacs configuration after almost a year of
using [Prelude](https://www.github.com/bbatsov/prelude). And when I tried
highlighting the Elisp code in it, the code highlighting started getting
messed up. Somehow it was not able to recognise Elisp as a form of Lisp.

Enter [Rouge](https://github.com/jneen/rouge). Well not just Rouge, but also
Jekyll's Github Flavoured Markdown rendering. Now powered by Github and Rouge's
highlighting I can write blog posts the same way I write README's and can safely
assume that code highlighting will be taken care of. Of course, it didn't solve
one of the pressing problems I had: the lack of themes for highlighting.
Fortunately I also found a Monokai based highlighting CSS for rouge.

So blog away, with better highlighting
