---
date: "2015-01-13T00:00:00Z"
title: From a Sublime Fanboy to an Emacs fanboy
---

It is said that a text editor is forever. It is a coder's first love and it is
with a text editor that the coder spends the maximum amount of time. For this
very reason, much like a car, a text editor is something that you modify to
provide you the maximum comfort in the long run. Well technically not just
modification but also all the bells and whistles of a text editor need to be
learned to get maximum productivity. Most coders invest hours in learning the
shortcuts techniques and in some cases the packages available for a text editor.
So naturally, most coders are reluctant(and rightly so) to move from one text
editor to another. It is a highly counter-productive excercise. This is one of
the main reasons I do not like coding for or in languages that demand the use of
an IDE or virtual-machine(read Java).

At the start of my career as a web developer I saw little use for anything more
than gedit on Ubuntu. I gradually moved to Bluefish for more support in using
PHP, especially snippets for both PHP and HTML. Within one year, I had graduated
to using Bootstrap for most of my projects and found that @mdo and @fat(authors
of Bootstrap), heavily favoured the use of Sublime Text 2 for development.
That was my first introduction to text editors on steroids.

The first thing that blew my mind when it came to Sublime Text 2 was the
multiple cursors. Those infinite lists could now be created/edited in jiffy.
Gradually I fell in love with the editor. It surprised me every single day as I
kept discovering new features, shortcuts and packages. Emmet for HTML generation,
temporary storage of files, sidebar navigation, project management - it all
seemed very awesome. Until I started using Sublime Text 3. It made my mind
explode. You could go to any file in a project, switch projects, auto-indent,
instant commenting, find and replace across files, drag lines up or down, not to
mention an even more awesome package control, with advanced linting for
different languages - all of this without almost no lag at all. I was confident
it couldn't get any better than this. I became a Sublime Text evangelist.

However it did not last long. During a discussion with some open source coders,
I began my usual evangelism of Sublime Text. I asked them if they were using
Sublime Text. I got a curt reply "It is not free" (It is proprietary, and also
paid). But surely they can use it as it is jam-packed with features and it has a
extended trial period. They listened to all that with a smug look on their face.
Then one of them quietly explained, "You don't need anything else when you have
Emacs"

I knew of Emacs. I knew it as an ancient editor used mostly at a time when no
GUIs existed. I was incredulous. "But all these features..", I began.
He replied, "Yes all these features, and more. It is an OS in itself."

That was the beginning of an incredible journey of discovery. The maze of myriad
major and minor modes combined with a plethora of shortcuts that led to more
more and more unbelievable features.

I went to the Emacs manual. It was indeed and ancient document, dusty and worn.
It seemed of very little use to a new age coder like me. So I went looking for
tutorials for beginners, and then I found my first package: Prelude. Prelude was
pretty much all I needed for the beginning. Prelude brought with it several
modes that would aid in running most of the languages I used. It was bundled
with Projectile that would handle project management and Magit, that would
handle git. That brought emacs to almost the same level as Sublime. But those
coders had promised more. So where was the awesomeness?

It didn't take me long to discover that. As I pored over blog post after blog
post about emacs features, I found you could run a terminal instance or if
needed multiple ones in the form of shells. Ah! one window less during
development. Well not just that, it had a default scribble pad, a package
manager in the form of MELPA, my favourite colour scheme Monokai and an
unofficial port of Emmet.

Some of the more esoteric features of emacs that I read about need to be tested
personally. From what I read, it is possible to read pdfs, emails, andI am not
sure if it true, play music from within emacs. Indeed if that is possible, I am
sure I would not need another window, once my desktop has been opened. All I
would need to do is to keep Emacs in full screen mode.
