---
date: "2016-02-16T00:00:00Z"
title: Starting out with Emacs
---

The Emacs stock UI is pathetic. There I said it. It is one the main reasons I
quit using Emacs several times before becoming a full time user. Add its
somewhat infamous key sequences for everything and a new user scurries back to
the comfort of the IDE he was using. If you can relate to this, I have a
solution for you. After all we are talking about one of the most extensible
editors out there. Emacs can be useful only if it is attuned to your needs.

Let's start out with configuring Emacs to some semblance of an advanced text
editor:

### The configuration file
Emacs configuration is stored in a `.emacs` in your home directory or inside
`.emacs.d/init.el`. Personally I prefer using the second option as it allows
you to add more custom configuration files if needed.

### Configuring the UI
Open `init.el`, preferably in Emacs. First let's remove the unnecessary
distraction that come enabled by default. Add these lines to the init file

```elisp
;; fix the emacs ui
(tool-bar-mode -1) ;; No toolbars
(blink-cursor-mode -1) ;; No blinking cursor
(scroll-bar-mode -1) ;; No ugly scrollbar
(setq-default cursor-type 'bar) ;; make cursor a line
```

Run `M-x eval-buffer` to see the effect

### Add a shortcut for eval-buffer
We will be running eval-buffer after every change we make and it can be a
tedious affair trying to type it all the time. So let's add a shortcut for it

```elisp
(eval-after-load "Emacs-Lisp"
  (define-key emacs-lisp-mode-map (kbd "C-c C-b") 'eval-buffer))
```

### Disable the splash screen
It's nice to see the welcome message and all the first time. Then it just gets
annoying. You can disable it by adding

```elisp
;; disable the splash screen
(setq inhibit-startup-message t)
```

### Enabling MELPA packages
Emacs 24 has a built-in package manager, but by default it will only show GNU
packages by default. A lion's share of the packages available for Emacs are
available from Milkypostman's Emacs Lisp Package Archive(or MELPA for short).
You can add that with these lines

```elisp
;; enable melpa packages
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
```

You can also add a shortcut to the packages list with these lines

```elisp
;; Package List shortcut
(global-set-key (kbd "C-x p") 'package-list-packages)
```

Once you are in the package list page you can see the shortcuts using `h`

### Install a theme
The default theme is too hard on the eyes. Some good themes you can install
from the list are *monokai*, *zenburn* and *spacemacs-theme*

### Modeline theme
The line at the bottom that shows all the info is called the modeline. Although
not necessary, it is possible to install themes for it. If you installed the
spacemacs-theme, I recommend installing the spaceline-theme. You can enable them
like this:

```elisp
;; load spacemacs theme
(load-theme 'spacemacs-dark t)

;; load the spaceline modeline theme
(require 'spaceline-config)
(spaceline-emacs-theme)
```

### Change the font
Having a sans typeface is easier on the eyes when coding. You can set custom
fonts and font-sizes using this line

```elisp
(custom-set-faces
 '(default ((t (:height 160 :family "Inconsolata")))))
```
