---
date: "2015-02-03T00:00:00Z"
title: Adding code highlighting to your jekyll blog
---
Jekyll it seems, is turning out to be the choice static site generator of
hackers. Although there is support for adding code, syntax highlighting depends
on the CSS you are using. A little googling churned up a gem of a library:
`highlight.js`

Adding highlight JS is a pretty simple affair. You can simply use a CDN or grab
the source and add it to your site. Personally I prefer CDNs. This is what you
should add to your footer to get easy and sophisticated code highlighting:

```html
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.4/styles/default.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.4/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
```

`highlight.js` also provides different CSS styles to produce different color
schemes. All you need to do is change the `default.min.css` to whatever color
scheme you want from this [list](https://github.com/isagalaev/highlight.js/tree/master/src/styles)
here. Don't forget to add the `.min` before the `.css`
