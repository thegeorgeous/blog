---
date: "2016-10-19T00:00:00Z"
title: Vagrant vs Docker
---

Vagrant vs Docker has been an increasingly debated topic for the past two years
or so. The lines are so blurred between these two that [this parody](https://circleci.com/blog/its-the-future/)
pretty much sums up what the most of the development community has been thinking.
DevOps guys have been trying to explain how Docker and containers are the future
of DevOps, while there has been a dedicated Vagrant community that swears [Vagrant
is not even competing with Docker](https://medium.com/@_marcos_otero/docker-vs-vagrant-582135beb623)

To understand the fundamental difference between the two, we need to understand
the problems these were intended to solve. Vagrant did not arise out of DevOps
problems. It rose out of developer problems, mostly the ubiquitous *'it worked on
my machine'* problem. Vagrant resolved this issue by providing a virtual machine
provisioned with all the necessary tools to faithfully replicate the production
environment on which the application would reside. Once the machine is created
on a local dev machine, it can be modified by the developer for easier
development, while still keeping the machine as close to production as possible.

Docker on the other hand solves an entirely different problem, which is,
production environment replication. Spinning up a new production environment
that is an exact clone of the previous environemnt is a tedious job. Vagrant and
its provisioning takes care of it, but what if the OS was different? Or just a
different flavour of Linux? Simply installing the necessary packages would be a
tedious process. Docker and containers solve the problem by creating silos of
necessary application and its dependencies. This effectively creates 'shippable
containers' that can simply be dropped into a cluster and is assured to work. It
also eliminates the need for elaborate provisioning scripts, since all the
'provisions' come with the containers.

This of course doesn't mean that Vagrant can't be used for spinning up new
production clusters or that Docker can't be used for development. Just that they
weren't really meant to do that.

A question that is often raised is which one should be used for development, or
at least what would warrant a preference of one over the other. As of now, Vagrant
is guaranteed to work for a team that uses different OSes but would want a uniform
dev environment. Docker *can* do that, but not as well as Vagrant. Docker on the
other hand eliminates the need for high-end specs in terms of RAM and CPU,
creates a much faster development environment as well as supports fast switching
between multiple projects. In short, Docker works well for individual developers
working on multiple projects and Vagrant works well for teams (especially remote
teams) looking for uniform dev environments.
