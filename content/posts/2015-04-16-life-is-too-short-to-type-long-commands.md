---
date: "2015-04-16T00:00:00Z"
title: Life is too short to type long commands
---
Are you Linux user? Ever had to type a long command into the terminal? Ever felt
that those Windows fanboys might be right? If you answered yes to at least one
question, you have come to the right place. So what next?

Most Linux newbies I know have hated the terminal. I wouldn't blame them. The
ones who jumped to the dark side for the right reasons, well, they love it at
first and then they whole-heartedly hate it. I wouldn't blame them either. When
you have to type the same long command over and over again, anyone would become
insane. Which brings me to the title: Don't type long commands more than once!

When Larry Wall famously listed *Laziness* as the first virtue of a programmer,
he meant in these situations. You should be lazy enough to not type it. Most
people save it in text files or some scratchpad app (or as I saw a colleague do
it : on Evernote) and then copy-paste it. I believe that is too much of of a
hassle, especially when Linux provides you such a beautiful alternative:
*aliases*

Aliases are as the name suggests alternative commands for commonly used
commands. One that is inbuilt in Linux is `l` for `ls`. You could write any
alias for any command as long as you be careful enough to not create clashes.

Creating aliases is extremely easy. First create a file in your home folder
called `.bash_aliases`. Note that the dot in the beginning will keep the file
hidden(Ctrl+H will show such files). Open the file in any text-editor. First we
will create a trivial alias. The ls command has several options and some options
have inbuilt aliases too. For example, `ls -a` is aliased as `la`. Let's create
an alias for `ls -ax`. `ls -as`  will list all files by lines as opposed to by
columns which is the default. To create this alias add the following line in
`.bash_aliases`.

```shell
alias 'ls -ax'='lx'
```

Now open a terminal and run this command

```shell
source ~/.bash_aliases
```

Now run `lx` and contrast it with `la`. Congratulations, you just created your
first alias.

Now alias away!!
