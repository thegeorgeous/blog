+++
title = "About me"
slug = "about"
+++

I am a software developer and consultant interested in continuous
delivery, process automation and DevSecOps. When not working on projects, I
like to learn new programming languages and write small hobby projects.

## Work
I've worked with several organizations both as employee and consultant and
helped teams develop software effectively. I've conducted workshops to help
developers incorporate tools like Git and Docker in their workflow and deliver
more value.

I've previously worked at [BangTheTable](https://www.bangthetable.com/) where
I worked on dockerizing the platform and in the ISO:27001 certification
process and GDPR compliance process. I've also worked at [Quintype](http://quintype.com/)
where my team built [Accesstype](https://www.quintype.com/products/accesstype),
a content monetization platform for news publishers.
